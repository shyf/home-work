const ul = document.createElement("ul");
let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
ul.innerHTML = arr.map(element => {
    return `<li>${element}</li>`
}).join("");
console.log(ul.innerHTML);
document.body.prepend(ul);
