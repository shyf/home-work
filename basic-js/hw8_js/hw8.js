const elemByParagraph = document.getElementsByTagName("p");
console.log(elemByParagraph);
for(let i = 0; i < elemByParagraph.length; i++) {
    elemByParagraph[i].style.backgroundColor = "#ff0000";
}
const elemById = document.getElementById("optionsList");
console.log(elemById);

const parentElemById = elemById.parentElement;
console.log(parentElemById);

const childNodes = elemById.childNodes;
for(let i = 0; i < childNodes.length; i++){
    console.log(childNodes[i].nodeName, childNodes[i].nodeType);
}
const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerHTML = "This is a paragraph";
console.log(testParagraph);

const mainHeader = document.querySelector(".main-header");
const elemsOfMainHeader = mainHeader.children;
console.log(elemsOfMainHeader);
for(let i = 0; i < elemsOfMainHeader.length; i++){
    elemsOfMainHeader[i].classList.add("nav-item");
    console.log(elemsOfMainHeader[i]);

}
const sectionTitle = document.getElementsByClassName("section-title");
for(let i = 0; i <sectionTitle.length; i++ ){
    const element = sectionTitle[i].remove("section-title");
}
console.log(sectionTitle);